define(['three'], function ( THREE ) {
    'use strict';

    // Variables for generator
    var appData = {

        // PARAMETERS
        boidsNumber:                50,

        maxVelocity:                6,

        groupQty:                   84,

        cohesionFactor:             0.2,

        // FLOCKING FUNCTION
        flockingUpdateFunction:     null,

        // HELPERS
        mouseVector:                new THREE.Vector3( 0,0,0 )
    };

    window.appData = appData;

    return appData;
});