define([
    'jquery',
    'underscore',
    'backbone',
    'three',
    'appData',
    'modules/boid',
    'collections/boids',
    'collections/buildings',
    'collections/tiles'
], function ( $, _, Backbone, THREE, appData, Boid, boids, buildings, tiles ) {
    'use strict';

    return {

        boidsColor: null,

        // Universal helpers
        getRandom: function ( min, max ) {
            return Math.random() * ( max - min ) + min;
        },

        // Scene helpers
        getDefaultCamera: function ( scene ) {
            var camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 10000 );
            camera.position.z = 1000;

            return camera;
        },

        getDefaultControls: function ( camera ) {
            return new THREE.TrackballControls( camera );
        },

        getDefaultScene: function () {
            var scene = new THREE.Scene();
            scene.fog = new THREE.FogExp2( 0xe5eef7, 0.0000000000000000000001 );

            return scene;
        },

        setLights: function ( scene ) {
            var light = new THREE.DirectionalLight( 0xffffff, 2 );
            light.position.set( 1, 1, 1 ).normalize();
            scene.add( light );

            var light = new THREE.DirectionalLight( 0xffffff );
            light.position.set( -1, -1, -1 ).normalize();
            scene.add( light );
        },

        // Boids helpers
        calculateRandomPosition: function() {
            return Math.floor( ( Math.random() * window.innerWidth/2 - window.innerWidth/3 ) / 10 ) * 10 + 5;
        },

        getBoid: function(i, geometry) {
            var self = this,
                boid, 
                boidColor = self.boidsColor ? self.boidsColor : Math.random() * 0xffffff;

            boid  = new Boid( i, new THREE.Vector3( self.getRandom( 0,200 ), self.getRandom( 0,100 ), 0 ) );
            boid.element =  new THREE.Mesh( geometry, new THREE.MeshLambertMaterial( { color: boidColor } ) );
            boid.element.position = self.getRandomPosition();

            return boid;
        },

        getRandomPosition: function () {
            var self = this,
                scale =  Math.floor( 0.21  * 2 + 1 ),
                position = new THREE.Vector3( 0, 0, 0 );

            position.x = self.calculateRandomPosition();
            position.z = ( scale * 50 ) / 2;
            position.y = self.calculateRandomPosition();

            return position;
        },

        drawDefaultGroup: function ( geometry, scene ) {
            var self = this;

            for ( var i = 1; i < appData.groupQty; i ++ ) {
                var boid = self.getBoid( i, geometry );
                
                boids.push( boid );
                scene.add( boid.element );
            }
        },

        drawBuilding: function ( geometry, position, scene ) {
            var self = this,
                building = new THREE.Mesh( geometry, new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff } ) );
            
            building.position = position;
            buildings.push( building );
            scene.add( building );
        },

        // A*
        pathCostEstimate: function (node, last) {
            var distanceX = last.mapX - node.mapX,
                distanceY = last.mapY - node.mapY;

            return Math.sqrt(distanceX*distanceX + distanceY*distanceY);
        },

        setPath: function ( x, y, cost, scene ) {
            var cubeMaterial = new THREE.MeshLambertMaterial( { color: 0xa3a3a3, overdraw: 0.5 });
            var cubeGeometry = new THREE.CubeGeometry( 50, 20, 50 );

            var nx = -(500 - (50/2)) + x*50;
            var nz = -(500 - (50/2)) + y*50;

            var newObject = null;
            newObject = new THREE.Mesh( cubeGeometry, cubeMaterial );
            newObject.position.x = Math.floor( nx / 50 ) * 50 + 25;
            newObject.position.y = Math.floor( 1 / 50 ) * 50 + 10;
            newObject.position.z = Math.floor( nz / 50 ) * 50 + 25;
            newObject.matrixAutoUpdate = false;
            newObject.updateMatrix();

            scene.add( newObject );
        },

        tailGetById: function (array, tile) {
            var self = array;
            for(var i in self) {
                if( self[i] &&
                    self[i].mapX && self[i].mapY &&
                    self[i].mapX === tile.mapX && self[i].mapY === tile.mapY) {
                    return self[i];
                }
            }

            return null;
        },

        tailRemove: function (array, tile) {
            var self = array;
            for(var i in self) {
                if(
                    self[i].mapX === tile.mapX && self[i].mapY === tile.mapY) {
                    self.splice(i,1);
                    return true;
                }
            }

            return false;
        },

        tailAdd: function (array, tile) {
            var self = array,
                found = false;

            for(var i in self) {
                if( self[i].id === tile.id ) {
                    found = true;
                    break;
                }
            }

            if(!found) {
                self.push(tile);
                return true;
            }

            return false;
        }
    }
});