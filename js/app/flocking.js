define(['three', 'appData', 'collections/specials', 'flock/tendMouse'], function ( THREE, appData, specials, TendMouse ) {

    var Flocking = function () {
        var self = this;

        // Flocking acc
        self.sum = new THREE.Vector3( 0,0,0 );
    };

    Flocking.prototype.update = function ( boid ) {
        var self = this;
        
        self.sum.x = 0;
        self.sum.y = 0;
        self.sum.z = 0;
       
        self.sum = appData.flockingUpdateFunction(boid);
        
        if ( specials.leader && specials.leader.id === boid.id ) {
            self.sum.add( TendMouse( boid ) );
        }
    };

    return Flocking;
});

