define( ['three', 'appData'], function ( THREE, appData ) {

    var Tile = function ( mapX, mapY ) {
        var self = this;

        self.id = mapX + 'x' + mapY;

        self.mapX = mapX;
        self.mapY = mapY;

        self.type = 'standard';
        self.neighbours = [];

        self.costFromStart = 0;
        self.costToGoal = 0;
        self.totalCost = 0;

        self.parent = null;
    }


    /**
     * Return bool is wall
     */
    Tile.prototype.isWall = function () {
        return this.type === 'wall';
    };


    /**
     * Set all neighbours
     */
    Tile.prototype.setNeighbours = function( tiles ) {
        var x = this.mapX,
            y = this.mapY;

        // on the left
        if ( x>0 ) {
            this.neighbours.push( tiles[x-1][y] );
        }

        // above
        if ( y>0 ) {
            this.neighbours.push( tiles[x][y-1] );
        }

        // on the right
        if ( x<19 ) {
            this.neighbours.push( tiles[x+1][y] );
        }

        // below
        if ( y<19 ) {
            this.neighbours.push( tiles[x][y+1] );
        }
    };


    /**
     * Set type of tail
     */
    Tile.prototype.setType = function ( type ) {
        this.type = type;
    };


    /**
     * Set Cost from Start
     */
    Tile.prototype.setCostFromStart = function (cost) {
        this.costFromStart = cost;
    };


    /**
     * Set Cost to Gloal
     */
    Tile.prototype.setCostToGoal = function (cost) {
        this.costToGoal = cost;
    };


    /**
     * Set Total cost
     */
    Tile.prototype.setTotalCost = function (cost) {
        this.totalCost = this.costFromStart + this.costToGoal;
    };


    /**
     * Set parent
     */
    Tile.prototype.setParent = function (parent) {
        if(!this.parent) {
            this.parent = parent;
        }
    };


    return Tile;
});