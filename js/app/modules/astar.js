define( ['three', 'appData', 'appHelpers', 'collections/tiles'], function ( THREE, appData, appHelpers, tiles ) {

    var Astar = function ( startLoc, goalLoc, scene ) {
        var guider = 0;

        openPath = [];
        closedPath = [];

        startLoc.setCostFromStart( 0 );
        startLoc.setCostToGoal( appHelpers.pathCostEstimate( startLoc, goalLoc ) );
        startLoc.setParent( null );
        startLoc.setTotalCost();
       
        appHelpers.tailAdd( openPath, startLoc );

        while ( openPath.length > 0 ) {

            if ( guider++>1000 ) {
                console.log( 'Nie udało się znaleźc poprawnej drogi' );
                break;
                return false;
            }

            var lowInd = 0;
            for( var i=0; i<openPath.length; i++ ) {

                if ( openPath[i].totalCost < openPath[lowInd].totalCost ) {
                    lowInd = i;
                }

            }

            var currentNode = openPath[lowInd];
            if ( currentNode.type === 'stop' ) {
                var curr = currentNode;
                var ret = [];
                while( curr.parent ) {

                    if ( curr.type == "start" ) {
                        break;
                    }

                    ret.push( curr );
                    appHelpers.setPath( curr.mapX, curr.mapY, 1, scene );
                    curr = curr.parent;
                }
                path = ret.reverse();
                break;
                return true;
            }

            appHelpers.tailRemove( openPath, currentNode );
            appHelpers.tailAdd( closedPath, currentNode );
            currentNode.setNeighbours( tiles );

            for ( var neighbour in currentNode.neighbours ) {
                var element = currentNode.neighbours[neighbour];

                if ( element.isWall() || appHelpers.tailGetById( closedPath, element ) ) {
                    continue;
                }
                
                var gScore = currentNode.costFromStart + 1;
                var gScoreIsBest = false;

                if ( !appHelpers.tailGetById( openPath, element ) ) {


                    gScoreIsBest = true;
                    element.setCostToGoal( appHelpers.pathCostEstimate( element, goalLoc ) );
                    appHelpers.tailAdd( openPath, element );
                }
                else if ( gScore < element.costToGoal ) {
                    gScoreIsBest = true;
                }

                if ( gScoreIsBest ) {
                    element.setParent( currentNode );
                    element.setCostFromStart( gScore );
                    element.setTotalCost();
                }
            }
        }

        return false;
    }

    return Astar;
});