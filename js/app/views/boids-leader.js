define([
    'jquery',
    'underscore',
    'backbone',
    'three',
    'appData',
    'appHelpers',
    'modules/boid',
    'collections/boids',
    'collections/buildings',
    'collections/specials',
    'flock/cohesion', 'flock/separation', 'flock/alignment', 'flock/avoidMouse', 'flock/avoidBuildings', 'flock/tendLeader'
], function ( $, _, Backbone, THREE, appData, appHelpers, Boid, boids, buildings, specials, Cohesion, Separation, Alignment, AvoidMouse, AvoidBuildings, TendLeader ) {
    'use strict';

    var BoidsLeader = Backbone.View.extend({

       // main objects
        scene               : null,
        camera              : null,
        controls            : null,

        events              : {
        },


        /**
         * Initialize view
         */
        initialize: function () {
            var self = this;

            // custom behaviour
            self.setCustomFlocking();

            // scene
            self.scene = appHelpers.getDefaultScene();

            // camera
            self.camera = appHelpers.getDefaultCamera();

            // controls
            self.controls = appHelpers.getDefaultControls( this.camera );

            // light
            appHelpers.setLights( this.scene );

            // propogate world
            self.drawWorld();

            return self;
        },


        _update: function () {
            return this;
        },


        _render: function () {
            var self = this;

            var timer = 0.0001 * Date.now();

            for (var i = 0; i < boids.length; i++) {
               boids[i].updateBoid();
            }

            return self;
        },


        /**
         * Set custom flocking behaviour
         */
        setCustomFlocking: function () {
            appData.flockingUpdateFunction = function ( boid ) {
                var vector = new THREE.Vector3( 0,0,0 );

                vector
                    .add( Cohesion( boid ) )
                    .add( Separation( boid ) )
                    .add( Alignment( boid ) )
                    .add( AvoidMouse( boid ) )
                    .add( AvoidBuildings( boid ) )
                    .add( TendLeader( boid ) )
                ;

                return vector;
            }
        },


        /**
         * Draw all world's objects
         */
        drawWorld: function () {
            appHelpers.drawBuilding( new THREE.CubeGeometry( 100,100,500 ), new THREE.Vector3( 0,0,0 ), this.scene );
            this.drawLeader();
            appHelpers.drawDefaultGroup( new THREE.CubeGeometry( 20,20,20 ), this.scene );
        },

        drawLeader: function () {
            var self= this,
                boid;

            appHelpers.boidsColor = 0xff8c00;
            boid = appHelpers.getBoid(0, new THREE.CubeGeometry( 20,20,20 ));
            appHelpers.boidsColor = 0xffffff;

            boids.push( boid );
            specials.leader = boid;
            self.scene.add(  boid.element );   
        }

    });

    return BoidsLeader;
});