define([
    'jquery',
    'underscore',
    'backbone',
    'three',
    'appData',
    'appHelpers',
    'modules/boid',
    'collections/boids',
    'flock/cohesion', 'flock/separation', 'flock/alignment', 'flock/avoidMouse', 'flock/avoidBuildings'
], function ($, _, Backbone, THREE, appData, appHelpers, Boid, boids, Cohesion, Separation, Alignment, AvoidMouse, AvoidBuildings) {
    'use strict';

    var BoidsBuildings = Backbone.View.extend({

        // main objects
        scene               : null,
        camera              : null,
        controls            : null,

        events              : {
        },


        /**
         * Initialize view
         */
        initialize: function () {
            var self = this;

            // custom behaviour
            self.setCustomFlocking();

            // scene
            self.scene = appHelpers.getDefaultScene();

            // camera
            self.camera = appHelpers.getDefaultCamera();

            // controls
            self.controls = appHelpers.getDefaultControls( this.camera );

            // light
            appHelpers.setLights( this.scene );

            // propogate world
            self.drawWorld();

            return self;
        },


        _update: function () {
            return this;
        },


        _render: function () {
            var self = this;

            var timer = 0.0001 * Date.now();

            for (var i = 0; i < boids.length; i++) {
               boids[i].updateBoid();
            }

            return self;
        },


        /**
         * Set custom flocking behaviour
         */
        setCustomFlocking: function () {
            appData.flockingUpdateFunction = function ( boid ) {
                var vector = new THREE.Vector3( 0,0,0 );

                vector
                    .add( Cohesion( boid ) )
                    .add( Separation( boid ) )
                    .add( Alignment( boid ) )
                    .add( AvoidMouse( boid ) )
                    .add( AvoidBuildings( boid ) )
                ;

                return vector;
            }
        },


        /**
         * Draw all world's objects
         */
        drawWorld: function () {
            appHelpers.drawBuilding( new THREE.CubeGeometry( 100,100,500 ), new THREE.Vector3( 0,0,0 ), this.scene );
            appHelpers.drawBuilding( new THREE.CubeGeometry( 100,100,500 ), new THREE.Vector3( 200,200,0 ), this.scene );
            appHelpers.drawBuilding( new THREE.CubeGeometry( 100,100,500 ), new THREE.Vector3( window.innerWidth-500, window.innerHeight/2,0 ), this.scene );
            appHelpers.drawBuilding( new THREE.CubeGeometry( 100,100,500 ), new THREE.Vector3( -window.innerWidth+300, window.innerHeight/2,0 ), this.scene );
            appHelpers.drawBuilding( new THREE.CubeGeometry( 100,100,500 ), new THREE.Vector3( -window.innerWidth/2 , -window.innerHeight/2,0 ), this.scene );
            appHelpers.drawDefaultGroup( new THREE.CubeGeometry( 20,20,20 ), this.scene );
        }

    });

    return BoidsBuildings;
});