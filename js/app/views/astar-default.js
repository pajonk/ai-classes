define([
    'jquery',
    'underscore',
    'backbone',
    'three',
    'appData',
    'appHelpers',
    'collections/tiles',
    'modules/tile',
    'modules/astar'
], function ( $, _, Backbone, THREE, appData, appHelpers, tiles, Tile, Astar ) {
    'use strict';


    var candidate = null,
        min = 0,
        path,
        openPath =  [],
        closedPath = [];
           

    var AstarView = Backbone.View.extend({

        self                : this,

        // canvas element
        el                  : '#app',
        canvas              : '#webgl',

        // main objects
        scene               : null,
        camera              : null,
        controls            : null,

        // mouse
        mouse               : new THREE.Vector2(),
        projector           : new THREE.Projector(),

        isCtrlDown          : false,
        isShiftDown         : false,

        startPlaced         : false,
        endPlaced           : false,

        startObject         : null,
        endObject           : null,

        tileStart           : null,
        tileStop            : null,

        events              : {            
            'mousemove  #webgl'     : 'onMouseMove',
            'mouseup    #webgl'     : 'mouseUp',
            'mousedown  #webgl'     : 'mouseDown',
            'keyup      '           : 'documentKeyUp',
            'keydown    '           : 'documentKeyDown',
            'resize     #webgl'     : 'windowResize'
        },


        initialize: function ( ) {
            var self = this;

            // scene
            self.scene = appHelpers.getDefaultScene();

            // camera
            self.setCamera();

            // controls
            self.controls = appHelpers.getDefaultControls( self.camera );

            // light
            self.setLights();

            // propogate world
            self.drawWorld();

            return this;
        },


        /**
         * Set custom background
         */
        setRenderer: function ( renderer ) {
            renderer.setClearColor( 0xfafafa, 0.2 );
            return renderer;
        },


        /**
         * Set custom camera
         */
        setCamera: function () {
            var self = this;

            self.camera             = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 3000 );
            self.camera.position.y  = 150;
            self.camera.position.z  = 500; 
        },


        /**
         * Set custom lights
         */
        setLights: function () {
            var self = this;

            self.scene.add( new THREE.AmbientLight( Math.random() * 0x10 ) );

            var light = new THREE.SpotLight(  Math.random() * 0xffffff, 0.5 );
            light.castShadow = true;
            light.position.set( 0, 500, 2000 );
            self.scene.add( light );

            var oppositeLight = new THREE.SpotLight( 0xffffff, 0.5 );
            oppositeLight.castShadow = true;
            oppositeLight.position.set( 0, 500, -2000 );
            self.scene.add( oppositeLight );
        },


        _update: function() {
            var vector = new THREE.Vector3( this.mouse.x, this.mouse.y, 0 );
            this.projector.unprojectVector( vector, this.camera );
            var ray = new THREE.Raycaster( this.camera.position, vector.sub( this.camera.position ).normalize() );
            var intersects = ray.intersectObjects( this.bricks );

            return this;
        },


        _render: function () {
            var self = this;

            return self;
        },


        /**
         * Draw custom world
         */
        drawWorld: function () {
            var self = this;

            // Propagate tiles
            for (var i = 0; i < 20; i++) {
                tiles[i] = [];
                for (var j = 0; j < 20; j++) {
                    tiles[i][j] = new Tile(i,j);
                }
            }

            self.drawGrid();
            self.drawWalls();  
        },


         /**
         * Draw grid
         */
        drawGrid: function () {
            var self = this,
                size = 500, step = 50,
                geometry = new THREE.Geometry(),
                material = new THREE.LineBasicMaterial( { color: 0x888888, opacity: 0.2 } ),
                line;

            for ( var i = - size; i <= size; i += step ) {
                geometry.vertices.push( new THREE.Vector3( - size, 0, i ) );
                geometry.vertices.push( new THREE.Vector3(   size, 0, i ) );
                geometry.vertices.push( new THREE.Vector3( i, 0, - size ) );
                geometry.vertices.push( new THREE.Vector3( i, 0,   size ) );
            }

            line = new THREE.Line( geometry, material );
            line.type = THREE.LinePieces;

            self.scene.add( line );
        },


        /**
         * Draw random map
         */
        drawWalls: function () {
            var self = this,
                plane, material, 
                mouse2D =  new THREE.Vector3( 0, 10000, 0.5 ),
                geometry = new THREE.CubeGeometry( 50, 50, 50 ),
                material = new THREE.MeshLambertMaterial( { color: 0xffffff, shading: THREE.FlatShading, overdraw: 0.5 } );

            plane = new THREE.Mesh( new THREE.PlaneGeometry( 1000, 1000 ), new THREE.MeshBasicMaterial() );
            plane.rotation.x = - Math.PI / 2;
            plane.visible = false;
            self.scene.add( plane );

            for ( var i = 0; i < 50; i ++ ) {
                var cube = new THREE.Mesh( geometry, material ),
                arrayX, arrayY;

                cube.scale.y = Math.floor(0.21 * 2 + 1 );
                cube.position.x = Math.floor( ( Math.random() * 1000 - 500 ) / 50 ) * 50 + 25;
                cube.position.y = ( cube.scale.y * 50 ) / 2;
                cube.position.z = Math.floor( ( Math.random() * 1000 - 500 ) / 50 ) * 50 + 25;
                self.scene.add( cube );

                arrayX = ( 500 - (50/2) + cube.position.x )/50;
                arrayY = ( 500 - (50/2) + cube.position.z )/50;
                tiles[arrayX][arrayY].setType('wall');
            }
        },


       /**
         * Set start & stop tiles on map
         */
        drawStartStop: function () {
            var self = this,
                vector, ray, normalMatrix, intersects, intersect,
                cubeGeometry =  new THREE.CubeGeometry( 50, 20, 50 ), 
                cubeMaterial = new THREE.MeshLambertMaterial( { color: 0x00ff80, overdraw: 0.5 });

            vector = new THREE.Vector3( self.mouse.x, self.mouse.y, 0 );
            self.projector.unprojectVector( vector, self.camera );
            ray = new THREE.Raycaster( self.camera.position, vector.sub( self.camera.position ).normalize() );
            normalMatrix = new THREE.Matrix3();
            intersects = ray.intersectObjects( self.scene.children );

            if ( intersects.length > 0 ) {
                intersect = intersects[ 0 ];

                if (self.isCtrlDown && intersects.length == 1) {

                    if(!self.startPlaced || !self.endPlaced) {
                        var normal, position, newObject,
                            arrayX, arrayY;

                        normalMatrix.getNormalMatrix( intersect.object.matrixWorld );

                        normal = intersect.face.normal.clone();
                        normal.applyMatrix3( normalMatrix ).normalize();

                        position = new THREE.Vector3().addVectors( intersect.point, normal );

                        newObject = new THREE.Mesh( cubeGeometry, cubeMaterial );
                        newObject.position.x = Math.floor( position.x / 50 ) * 50 + 25;
                        newObject.position.y = Math.floor( position.y / 50 ) * 50 + 10;
                        newObject.position.z = Math.floor( position.z / 50 ) * 50 + 25;
                        newObject.matrixAutoUpdate = false;
                        newObject.updateMatrix();


                        arrayX = ( 500 - (50/2) + newObject.position.x )/50;
                        arrayY = ( 500 - (50/2) + newObject.position.z )/50;
                    }

                    if( !self.startPlaced ) {
                        self.startObject = newObject;
                        self.scene.add( self.startObject );

                        self.tileStart = tiles[arrayX][arrayY];
                        self.tileStart.setType( 'start' );

                        self.startPlaced = true;
                    }
                    else if ( !self.endPlaced ) {
                        self.endObject = newObject;
                        self.scene.add( self.endObject );

                        self.tileStop = tiles[arrayX][arrayY];
                        self.tileStop.setType( 'stop' );

                        self.endPlaced = true; 

                        new Astar( self.tileStart, self.tileStop, self.scene );
                    }
                }
            }
        },

        /**
         * Mouse up Bind
         */
        mouseUp: function () {
            this.drawStartStop();
        },


        /**
         * Mouse move Bind
         */
        onMouseMove: function(e) {
            this.mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
            this.mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;
        },


        /**
         * KeyDown Bind
         */
        documentKeyDown: function ( event ) {
            switch( event.keyCode ) {

                case 16: this.isShiftDown = true; break;
                case 17: this.isCtrlDown = true; break;

            }
        },


        /**
         * KeyUp Bind
         */
        documentKeyUp: function ( event ) {
            switch( event.keyCode ) {
                case 16: this.isShiftDown = false; break;
                case 17: this.isCtrlDown = false; break;
            }
        }

    });

    return AstarView;
});