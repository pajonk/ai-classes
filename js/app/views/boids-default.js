define([
    'jquery',
    'underscore',
    'backbone',
    'three',
    'appData',
    'appHelpers',
    'modules/boid',
    'collections/boids',
    'flock/cohesion', 'flock/separation', 'flock/alignment'
], function ($, _, Backbone, THREE, appData, appHelpers, Boid, boids, Cohesion, Separation, Alignment) {
    'use strict';

    var BoidsDefault = Backbone.View.extend({
       // main objects
        scene               : null,
        camera              : null,
        controls            : null,

        events              : {
        },


        /**
         * Initialize view
         */
        initialize: function () {
            var self = this;

            // custom behaviour
            self.setCustomFlocking();

            // scene
            self.scene = appHelpers.getDefaultScene();

            // camera
            self.camera = appHelpers.getDefaultCamera();

            // controls
            self.controls = appHelpers.getDefaultControls( this.camera );

            // light
            appHelpers.setLights( this.scene );

            // propogate world
            self.drawWorld();

            return self;
        },


        _update: function () {
            return this;
        },


        _render: function ( ) {
            var self = this;

            var timer = 0.0001 * Date.now();

            for (var i = 0; i < boids.length; i++) {
               boids[i].updateBoid();
            }

            return self;
        },


        /**
         * Set custom flocking behaviour
         */
        setCustomFlocking: function () {
            appData.flockingUpdateFunction = function ( boid ) {
                var vector = new THREE.Vector3( 0,0,0 );

                vector
                    .add( Cohesion  ( boid ) )
                    .add( Separation( boid ) )
                    .add( Alignment ( boid ) )
                ;

                return vector;
            }
        },


        /**
         * Draw all world's objects
         */
        drawWorld: function () {
            appHelpers.drawDefaultGroup( new THREE.CubeGeometry( 20,20,20 ), this.scene );
        }

    });

    return BoidsDefault;
});